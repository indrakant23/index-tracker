const puppeteer = require("puppeteer");

var position = 0;
var browser = null;
var page = null;

async function getData() {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector("div.r > a");

      var links = await page.evaluate(() => {
        var search = document.querySelectorAll("div.r > a");
        var links = [];
        for (var i = 0; i < search.length; i++) {
          links.push(search[i].getAttribute("href"));
        }
        return links;
      });

      for (var i = 0; i < links.length; i++) {
        if (links[i].indexOf("getmeroof.com") != -1) {
          resolve({ position: position, url: links[i] });
        }
        else {
          position += 1;
        }
      }

      if (position < 100) {
        await page.evaluate(() => {
          document.getElementById("pnnext").click();
        })
        var result = await getData();
        resolve(result);
      }
      else {
        return resolve({ position: null, url: null });
      }

    } catch (err) {
      console.log(err);
      await browser.close();
      return resolve({ position: null, url: null });
    }
  })

}


async function main() {
  browser = await puppeteer.launch({ headless: false });
  page = await browser.newPage();
  await page.goto("https://google.com");
  await page.focus(".gLFyf");
  await page.keyboard.type(process.argv[2]);
  await page.keyboard.press('Enter');
  var result = await getData();
  await browser.close();
  console.log(result);
}

main();
